package com.example.ExamSoap.soap.repository;

import com.example.ExamSoap.soap.bean.Item;
import com.example.ExamSoap.soap.bean.Suppliers;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Suppliers, Integer> {
}
