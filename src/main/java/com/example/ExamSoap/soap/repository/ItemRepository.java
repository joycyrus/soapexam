package com.example.ExamSoap.soap.repository;

import com.example.ExamSoap.soap.bean.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {
}
