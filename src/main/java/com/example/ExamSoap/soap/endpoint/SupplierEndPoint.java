package com.example.ExamSoap.soap.endpoint;

import com.example.ExamSoap.soap.model.Supplier;
import com.example.ExamSoap.soap.repository.SupplierRepository;
import com.example.ExamSoap.suppliers.CreateSuppliersRequest;
import com.example.ExamSoap.suppliers.GetSuppliersResponse;
import com.example.ExamSoap.suppliers.Suppliers;
import com.example.ExamSoap.suppliers.UpdateSuppliersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.Optional;

public class SupplierEndPoint {
    @Autowired
    private SupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://rca.ac.rw/souvede/suppliers",localPart = "GetSuppliersRequest");
    @ResponsePayload
    public GetSuppliersResponse findById(@RequestPayload GetCourseDetailsRequest request) {

        Supplier course = supplierRepository.findById(request.getId()).get();

        GetSuppliersResponse courseDetailsResponse = mapCourseDetails(course);
        return  courseDetailsResponse;
    }

    @PayloadRoot(namespace = "http://schoolsoapapi.edu.com/courses", localPart = "CreateCourseDetailsRequest")
    @ResponsePayload
    public CreateSuppliersResponse save(@RequestPayload CreateSuppliersRequest request) {
        supplierRepository.save();
        CreateSuppliersResponse courseDetailsResponse = new CreateCourseDetailsResponse();
        courseDetailsResponse.setCourseDetails(request.getCourseDetails());
        courseDetailsResponse.setMessage("Created Successfully");
        return courseDetailsResponse;
    }

    @PayloadRoot(namespace = "http://schoolsoapapi.edu.com/courses", localPart = "UpdateCourseDetailsRequest")
    @ResponsePayload
    public UpdateSupplierResponse update(@RequestPayload UpdateSuppliersRequest request) {
        UpdateCourseDetailsResponse courseDetailsResponse = null;
        Optional<Course> existingCourse = this.courseRepository.findById(request.getCourseDetails().getId());
        if(existingCourse.isEmpty() || existingCourse == null) {
            courseDetailsResponse = mapCourseDetail(null, "Id not found");
        }
        if(existingCourse.isPresent()) {

            Course _course = existingCourse.get();
            _course.setName(request.getCourseDetails().getName());
            _course.setDescription(request.getCourseDetails().getDescription());
            courseRepository.save(_course);
            courseDetailsResponse = mapCourseDetail(_course, "Updated successfully");

        }
        return courseDetailsResponse;
    }

    @PayloadRoot(namespace = "http://schoolsoapapi.edu.com/courses", localPart = "DeleteCourseDetailsRequest")
    @ResponsePayload
    public DeleteSuppliersResponse save(@RequestPayload DeleteCourseDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        courseRepository.deleteById(request.getId());

        DeleteCourseDetailsResponse courseDetailsResponse = new DeleteCourseDetailsResponse();
        courseDetailsResponse.setMessage("Deleted Successfully");
        return courseDetailsResponse;
    }

    private GetCourseDetailsResponse mapCourseDetails(Course course){
        CourseDetails courseDetails = mapCourse(course);

        GetSuppliersResponse courseDetailsResponse = new GetSupplierResponse();

        courseDetailsResponse.setCourseDetails(courseDetails);
        return courseDetailsResponse;
    }

    private UpdateCourseDetailsResponse mapCourseDetail(Supplier course, String message) {
        Supplier courseDetails = mapCourse(course);
        UpdateSuppliersResponse courseDetailsResponse = new UpdateSuppliersResponse();

        courseDetailsResponse.setCourseDetails(courseDetails);
        courseDetailsResponse.setMessage(message);
        return courseDetailsResponse;
    }

    private CourseDetails mapCourse(Course course){
        Supplier courseDetails = new Suppliers();
        courseDetails.setDescription(course.getDescription());
        courseDetails.setId(course.getId());
        courseDetails.setName(course.getName());
        return courseDetails;
    }
}
}
