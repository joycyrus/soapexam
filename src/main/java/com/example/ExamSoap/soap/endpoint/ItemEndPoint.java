package com.example.ExamSoap.soap.endpoint;

import com.edu.schoolsoapapi.courses.*;
import com.edu.schoolsoapapi.soap.repository.ICourseRepository;
import com.example.ExamSoap.items.DeleteItemsRequest;
import com.example.ExamSoap.items.GetItemsResponse;
import com.example.ExamSoap.items.Items;
import com.example.ExamSoap.items.UpdateItemsResponse;
import com.example.ExamSoap.soap.bean.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemEndPoint {

    @Autowired
    private ItemRepository courseRepository;

    @PayloadRoot(namespace = "http://schoolsoapapi.edu.com/courses", localPart = "GetCourseDetailsRequest")
    @ResponsePayload
    public GetItemsResponse findById(@RequestPayload GetItemsRequest request) {

        Item course = courseRepository.findById(request.getId()).get();

        GetItemsResponse courseDetailsResponse = mapCourseDetails(course);
        return  courseDetailsResponse;
    }

    @PayloadRoot(namespace = "http://schoolsoapapi.edu.com/courses", localPart = "GetAllCourseDetailsRequest")
    @ResponsePayload
    public GetAllItemsResponse findAll(@RequestPayload GetAllItemsRequest request){
        GetAllItemsResponse allCourseDetailsResponse = new GetAllItemsResponse();

        List<Item> courses = courseRepository.findAll();
        for (Item course: courses){
            GetItemsResponse courseDetailsResponse = mapCourseDetails(course);
            allCourseDetailsResponse.getCourseDetails().add(courseDetailsResponse.getCourseDetails());
        }
        return allCourseDetailsResponse;
    }


    @PayloadRoot(namespace = "http://schoolsoapapi.edu.com/courses", localPart = "CreateCourseDetailsRequest")
    @ResponsePayload
    public CreateItemsResponse save(@RequestPayload CreateCourseDetailsRequest request) {
        courseRepository.save(new Item(request.getCourseDetails().getId(),
                request.getItems().getName(),
                request.getItems().getDescription()
        ));

        CreateItemsResponse courseDetailsResponse = new CreateItemsResponse();
        courseDetailsResponse.setCourseDetails(request.getCourseDetails());
        courseDetailsResponse.setMessage("Created Successfully");
        return courseDetailsResponse;
    }

    @PayloadRoot(namespace = "http://schoolsoapapi.edu.com/courses", localPart = "UpdateCourseDetailsRequest")
    @ResponsePayload
    public UpdateItemsResponse update(@RequestPayload UpdateItemsRequest request) {
        UpdateItemsResponse courseDetailsResponse = null;
        Optional<Item> existingCourse = this.courseRepository.findById(request.getCourseDetails().getId());
        if(existingCourse.isEmpty() || existingCourse == null) {
            courseDetailsResponse = mapCourseDetail(null, "Id not found");
        }
        if(existingCourse.isPresent()) {

            Item _course = existingCourse.get();
            _course.setName(request.getCourseDetails().getName());
            _course.setDescription(request.getCourseDetails().getDescription());
            courseRepository.save(_course);
            courseDetailsResponse = mapCourseDetail(_course, "Updated successfully");

        }
        return courseDetailsResponse;
    }

    @PayloadRoot(namespace = "http://schoolsoapapi.edu.com/courses", localPart = "DeleteCourseDetailsRequest")
    @ResponsePayload
    public DeleteItemsResponse save(@RequestPayload DeleteItemsRequest request) {

        System.out.println("ID: "+request.getId());
        courseRepository.deleteById(request.getId());

        DeleteItemsResponse courseDetailsResponse = new DeleteItemsResponse();
        courseDetailsResponse.setMessage("Deleted Successfully");
        return courseDetailsResponse;
    }

    private GetItemsResponse mapCourseDetails(Item course){
        Item courseDetails = mapCourse(course);

        GetItemsResponse courseDetailsResponse = new GetItemsResponse();

        courseDetailsResponse.setCourseDetails(courseDetails);
        return courseDetailsResponse;
    }

    private UpdateItemsResponse mapCourseDetail(Item course, String message) {
        Item courseDetails = mapCourse(course);
        UpdateItemsResponse courseDetailsResponse = new UpdateItemsResponse();

        courseDetailsResponse.setCourseDetails(courseDetails);
        courseDetailsResponse.setMessage(message);
        return courseDetailsResponse;
    }

    private Item mapCourse(Item course){
        Item courseDetails = new Item();
        courseDetails.setDescription(course.getDescription());
        courseDetails.setId(course.getId());
        courseDetails.setName(course.getName());
        return courseDetails;
    }
}
