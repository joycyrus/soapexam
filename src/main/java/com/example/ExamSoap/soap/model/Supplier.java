package com.example.ExamSoap.soap.model;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity(name = "suppliers")
public class Supplier {
    @Id
    String id;
    String names;
    String email;
    String mobile;
    Supplier(String nam,String mobile){
        this.names = nam;
        this.mobile = mobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
