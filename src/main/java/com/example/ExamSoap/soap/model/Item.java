package com.example.ExamSoap.soap.model;


import com.example.ExamSoap.soap.model.Supplier;
import com.example.ExamSoap.soap.enums.StatusEnum;
import com.sun.istack.NotNull;

import javax.persistence.*;


@Entity(name="items")
public class Item {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String itemCode;

    private StatusEnum status;

    private float price;

    @OneToMany(fetch = FetchType.LAZY)
    private Supplier suppliers;

    public Item(String name, String code) {
        this.name=name;
        this.itemCode=code;
    }

    public Item() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }



    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Supplier getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(Supplier suppliers) {
        this.suppliers = suppliers;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }
//    @Override
//    public String toString() {
//        return String.format("Course [id=%s, name=%s, description=%s]", id, name);
//    }

}

